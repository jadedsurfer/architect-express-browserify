"use strict";
var middleware = require("browserify-middleware");

module.exports = function(options, imports, register) {
  var debug = imports.debug("express:middleware:browserify");
  debug("start");

  debug("register nothing");
  register(null, {
    browserify: middleware
  });
};
